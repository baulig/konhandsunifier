#!/usr/bin/env python3

import os
import json


PWD = os.path.dirname(os.path.realpath(__file__))
PROJECT = os.path.join(PWD, 'KonHandsUnifier.wrap')
BASE_MESH = os.path.join(PWD, 'Base', 'RightHand.obj')
SCAN_MESH = os.path.join(PWD, 'Scan', 'ScanMesh.obj')
SCAN_TEX = os.path.join(PWD, 'Scan', 'ScanTex.jpg')
OUTPUT = os.path.join(PWD, 'Unified')
NAME = 'Unified'


def argparser(parents=None, prog=__file__):
	from argparse import ArgumentParser
	parser = ArgumentParser(
		parents=parents if parents else [],
		prog=prog
	)
	parser.add_argument('-p', '--project', help="The Wrap3.3 project file", type=str, default=PROJECT, metavar='WRAP')
	parser.add_argument('-b', '--base', help="Wavefront OBJ for BaseMesh", type=str, default=BASE_MESH, metavar='OBJ')
	parser.add_argument('-s', '--scan', help="Wavefront OBJ for ScanMesh", type=str, default=SCAN_MESH, metavar='OBJ')
	parser.add_argument('-t', '--tex', help="Texture for ScanTex", type=str, default=SCAN_TEX, metavar='IMAGE')
	parser.add_argument('-o', '--output', help="Output dir for unified objects", type=str, default=OUTPUT, metavar='DIR')
	parser.add_argument('-n', '--name', help="Name for unified objects", type=str, default=NAME, metavar='STRING')
	parser.add_argument('-f', '--format', help="Output image format", type=str, choices=['jpg', 'png'], default='jpg', metavar='FORMAT')
	return parser

	
def main(args):
	with open(args.project, 'r') as fid:
		project = json.load(fid)
		pass
	
	outmesh = os.path.realpath(os.path.join(args.output, '{}.{}'.format(args.name, 'obj')))
	outtex = os.path.realpath(os.path.join(args.output, '{}.{}'.format(args.name, args.format)))
	
	project['nodes']['BaseMesh']['params']['fileNames']['value'][0] = os.path.realpath(args.base)
	project['nodes']['ScanMesh']['params']['fileNames']['value'][0] = os.path.realpath(args.scan)
	project['nodes']['LoadTex']['params']['fileNames']['value'][0] = os.path.realpath(args.tex)
	project['nodes']['SaveMesh']['params']['fileName']['value'] = outmesh
	project['nodes']['SaveTex']['params']['fileName']['value'] = outtex
	
	with open(args.project, 'w') as fid:
		json.dump(project, fid, indent=4)
		pass
	

if __name__ == '__main__':
	args, _ = argparser().parse_known_args()
	main(args)